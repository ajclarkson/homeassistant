# HomeAssistant Configuration

:house_with_garden: This is my collection of configuration files for [homeassistant](https://home-assistant.io/)

## Software

All of the software runs on my [rancher](https://rancher.com/) server in docker containers:

- [HomeAssistant](https://home-assistant.io/)
- [Dasher](https://github.com/maddox/dasher) via the [maaso/dasher image](https://hub.docker.com/r/maaso/dasher/)
- [eclipse-mosquitto](https://github.com/eclipse/mosquitto) for MQTT
- [hue-mqtt-bridge](https://github.com/dale3h/hue-mqtt-bridge) for monitoring hue sensors over mqtt (I created the [ajclarkson/hue-mqtt-bridge](https://hub.docker.com/r/ajclarkson/hue-mqtt-bridge/) image to run this in docker)
- The excellent [ha-floorplan](https://github.com/pkozul/ha-floorplan) to tie everything into a neat visualisation
- [python-firetv](https://hub.docker.com/r/sytone/python-firetv/) to enable the Amazon FireTV as a media player in HA

## Current Devices

- Phillips Hue Bridge (2nd Gen)
- Collection of Hue bulbs
- Phillips Hue Motion sensors
- TP-Link H100 Switches
- Amazon Echo Dot
- Amazon Dash Button
- Denon HEOS-1 Speaker (Not Yet Hooked Up)
- Amazon FireTV

## Automations

- Use a union of the motion sensors states, and times we would usually move around to detect if we are away from home, and activate "away mode" if we are.

- Use the light sensors in the hue motion sensors to turn some lamps on in an evening

- Vacation lights activate if away mode is on and it is evening

- Night mode - currently activated by Alexa - turn on bedroom lamps and hallway, disable bedroom motion sensor automations

- Multipurpose bedroom dash button - if its night mode and not in guest mode, turn off everything in the house. Otherwise, just toggle the bedroom lamps

- Use bedroom motion sensor to turn lights on if required (based on light level), disabled by night mode, and re-enabled at 10am

- Activating movie mode sets the lights and turns the TV on and to the fire stick
