function temperatureTile(id, position, title, min = 20, max = 24) {
   return {
       position: position,
       type: TYPES.SENSOR,
       id: id,
       title,
       unit: '°C',
       state: ' '
   };
}

function batteryTile(id, position, title) {
   return {
      position,
      type: TYPES.SENSOR_ICON,
      id,
      title,
      filter: function (value) {
         return value < 25 ? 'Low' : value > 90 ? 'High' : 'Medium';
      },
      icon: function (item, entity) {
         var amount = Math.ceil(entity.state / 10) * 10;
         return amount === 100 ? 'mdi-battery' : 'mdi-battery-' + amount;
      },
      state: function (item, entity) {
         return entity.state + entity.attributes.unit_of_measurement;
      }
   }
}

function presenceTile(id, position, title) {
   return {
      position,
      id,
      title,
      type: TYPES.SENSOR_ICON,
      icons: {
         on: 'mdi-check-circle',
         off: 'mdi-checkbox-blank-circle-outline'
      }
   }
}

function lightLevelTile(id, position, title, low = 7000, high = 12000) {
   return {
      position,
      type: TYPES.SENSOR,
      id,
      title,
      filter: function (value) {
         return value < low ? 'Low' : value < high ? 'Ok' : 'Bright';
      },
      unit: '',
      state: function (item, entity) {
         const unit = entity.attributes.unit_of_measurement || ''
         return `${entity.state} ${unit}`;
      }

   }
}

function percentageSlider(id, position, title){
   return {
      position,
      width: 1,
      height: 3.5,
      type:TYPES.SLIDER,
      id,
      title,
      max: 100,
      unit: '%',
      state: false,
      min: 0,
      step: 1,
      icon:  'mdi-flash',
      request: {
         type: "call_service",
         domain: "input_number",
         service: "set_value",
         field: "value"
      }
   }
}

function playlistTile(id, position, icon) {
   return {
      position,
      type: TYPES.SWITCH,
      id,
      state: false,
      icons: {
         on: icon,
         off: icon
      }
   }
}

function switchTile(id, position, title, iconOn, iconOff) {
   return {
      position,
      id,
      title,
      type: TYPES.SWITCH,
      icons: {
         on: iconOn,
         off: iconOff
      },
      states: ON_OFF_STATES

   }
}

function temperatureGaugeTileLarge(id, position, title) {
   return {
      position,
      width: 2,
      height: 1.5,
      title,
      subtitle: '',
      type: TYPES.GAUGE,
      id, // Assign the sensor you want to display on the gauge
      value: function(item, entity){
         return entity.state;
      },
      state: '',
      settings: {
         // size: 100, // Defaults to 50% of either height or width, whichever is smaller
         type: 'arch', // Options are: 'full', 'semi', and 'arch'. Defaults to 'full'
         min: 0, // Defaults to 0
         max: 35, // Defaults to 100
         cap: 'round', // Options are: 'round', 'butt'. Defaults to 'butt'
         thick: 6, // Defaults to 6
         append: '@attributes.unit_of_measurement', // Defaults to undefined
         duration: 600, // Defaults to 1500ms
         thresholds: { 0: { color: 'green'}, 22: { color: 'orange' }, 25: { color: 'red'} },  // Defaults to undefined
         labelOnly: false, // Defaults to false
         foregroundColor: 'rgba(0, 150, 136, 1)', // Defaults to rgba(0, 150, 136, 1)
         backgroundColor: 'rgba(0, 0, 0, 0.1)', // Defaults to rgba(0, 0, 0, 0.1)
         fractionSize: 0, // Number of decimal places to round the number to. Defaults to current locale formatting
      },
   }
}

function sensorIconTile(id, position, title, iconOn, iconOff) {
   return {
      position,
      id,
      title,
      type: TYPES.SENSOR_ICON,
      icons: {
         on: iconOn,
         off: iconOff
      },
      states: ON_OFF_STATES
   }
}

const ON_OFF_STATES = { on: 'On', off: 'Off'};


var CONFIG = {
   customTheme: CUSTOM_THEMES.HOMEKIT, // CUSTOM_THEMES.TRANSPARENT, CUSTOM_THEMES.MATERIAL, CUSTOM_THEMES.MOBILE, CUSTOM_THEMES.COMPACT, CUSTOM_THEMES.HOMEKIT, CUSTOM_THEMES.WINPHONE, CUSTOM_THEMES.WIN95
   transition: TRANSITIONS.SIMPLE, //ANIMATED or SIMPLE (better perfomance)
   entitySize: ENTITY_SIZES.SMALL, //SMALL, BIG are available
   tileSize: 130,
   tileMargin: 15,
   groupMarginCss: "2.5px",
   serverUrl: "http://home.kube.local",
   wsUrl: "ws://home.kube.local/api/websocket",
   authToken: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJkMzE0MzdkYzk3ZDc0ZDI3YTEwM2I3YzA3ZTZlMThiYiIsImlhdCI6MTU5MjY1NjI3NSwiZXhwIjoxOTA4MDE2Mjc1fQ.uOp4G6iHqxszcfxsgIvlxa8xxRbsQZvOfNKRmCHbqeg", // optional: make an long live token and put it here
   //googleApiKey: "XXXXXXXXXX", // Required if you are using Google Maps for device tracker
   debug: false, // Prints entities and state change info to the console.

   // next fields are optional
   events: [],
   timeFormat: 24,
   menuPosition: MENU_POSITIONS.LEFT, // or BOTTOM
   hideScrollbar: false, // horizontal scrollbar
   groupsAlign: GROUP_ALIGNS.HORIZONTALLY, // or VERTICALLY

   pages: [
      {
         title: 'Main page',
         icon: 'mdi-home-outline',
         groups: [
            {
               width: 5,
               height: 3.5,
               items: [
                  percentageSlider('input_number.living_room_brightness', [0,0], 'Lights'),
                  temperatureGaugeTileLarge('sensor.living_room_sensor_temperature', [1,0], 'Room Temperature'),
                  switchTile('input_boolean.evening_lamps_enabled', [1, 1.5], 'Automated Lights', 'mdi-weather-sunset', 'mdi-weather-sunset'),
                  switchTile('input_boolean.guest_mode', [2, 1.5], 'Guest Mode', 'mdi-account-circle', 'mdi-account-circle'),
                  switchTile('input_boolean.sonos_follow', [1,2.5], 'Sonos Follow', 'mdi-music', 'mdi-music-off'),
                  {
                     position: [3, 0],
                     height: 1.5,
                     width: 2,
                     title: 'Current Weather',
                     type: TYPES.WEATHER,
                     id: 'group.weather',
                     state: false,
                     icon: '&sensor.dark_sky_icon.state',
                     icons: {
                        'clear-day': 'clear',
                        'clear-night': 'nt-clear',
                        'cloudy': 'cloudy',
                        'rain': 'rain',
                        'sleet': 'sleet',
                        'snow': 'snow',
                        'wind': 'hazy',
                        'fog': 'fog',
                        'partly-cloudy-day': 'partlycloudy',
                        'partly-cloudy-night': 'nt-partlycloudy'
                     },
                     fields: {
                        summary: '&sensor.dark_sky_summary.state',
                        temperature: '&sensor.dark_sky_temperature.state',
                        temperatureUnit: '&sensor.dark_sky_temperature.attributes.unit_of_measurement',
                        list: [
                           '&sensor.dark_sky_summary.state',
                           `\n`,
                           
                           'Feels like '
                              + '&sensor.dark_sky_apparent_temperature.state'
                              + '&sensor.dark_sky_apparent_temperature.attributes.unit_of_measurement',
                           '&sensor.dark_sky_precip_probability.state'
                              + '&sensor.dark_sky_precip_probability.attributes.unit_of_measurement'
                              + ' chance of rain',
                           '&sensor.dark_sky_humidity.state'
                              + '&sensor.dark_sky_humidity.attributes.unit_of_measurement'
                              + ' Humidity'
                        ]
                     }
                  },
                  switchTile('input_boolean.kitchen_speaker', [2,2.5], 'Kitchen Speaker', 'mdi-speaker', 'mdi-speaker-off'),
                  switchTile('input_boolean.bedroom_speaker', [3,2.5], 'Bedroom Speaker', 'mdi-speaker', 'mdi-speaker-off'),
                  sensorIconTile('binary_sensor.updater', [4,2.5], 'Update Available', 'mdi-check-circle', 'mdi-checkbox-blank-circle-outline')
                  
               ]
            }
         ]
         
      },
      {
         title: 'Bedroom',
         icon: 'mdi-hotel',
         groups: [
            {
               title: '',
               width: 5,
               height: 3.5,
               items: [
                  percentageSlider('input_number.bedroom_brightness', [0,0], 'Lights'),
                  temperatureGaugeTileLarge('sensor.bedroom_sensor_temperature', [1,0], 'Room Temperature'),
                  switchTile('input_boolean.bedtime', [1, 1.5], 'Bedtime', 'mdi-sleep', 'mdi-sleep'),
                  switchTile('switch.bedroom_fan', [2,1.5], 'Fan', 'mdi-fan', 'mdi-fan-off'),
                  sensorIconTile('binary_sensor.bed_occupancy', [1,2.5], 'Bed Occupancy', 'mdi-hotel', 'mdi-bed-empty'),
                  switchTile('input_boolean.bedroom_lights_enabled', [2,2.5], 'Motion Lights', 'mdi-eye', 'mdi-eye-off')
               ]
            }
         ]
      },
   ],
}
